package me.ray.utils;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class InventoryUtils {

    public static String serealizeInv(Map<Integer, ItemStack> inventory) {
        String serialized = "";

        for(int slot : inventory.keySet()) {
            serialized += slot + ":" + ItemUtils.toString(inventory.get(slot)) + ", ";
        }

        return serialized.substring(0, serialized.length() - 2);
    }

    public static Map<Integer, ItemStack> deseralizeInv(String serialized) {
        Map<Integer, ItemStack> inv = new HashMap<>();

        String[] rawData = serialized.split(", ");

        for(String raw : rawData) {
            String[] data = raw.split(":");
            int slot = Integer.valueOf(data[0]);
            ItemStack item = ItemUtils.fromString(data[1]);
            inv.put(slot, item);
        }

        return inv;
    }

}
