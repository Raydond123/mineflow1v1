package me.ray.utils;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    final String PATH = "plugins/Mineflow1v1";

    public static FileConfiguration getFile(String fileName) {
        try {
            return YamlConfiguration.loadConfiguration(getFile(fileName, true));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static File getFile(String fileName, boolean createIfNotExist)
            throws IOException {
        File file = new File(PATH, fileName);
        if (createIfNotExist && !file.exists()) {
            boolean success = file.createNewFile();
            if (!success)
                throw new IOException("File couldn't be created");
        }
        return file;
    }

    public static void saveFile(FileConfiguration file, String name) {
        try {
            file.save(getFile(name, false));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
