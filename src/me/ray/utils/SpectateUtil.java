package me.ray.utils;

import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SpectateUtil implements Listener {

    private List<UUID> spectating = new ArrayList<>();

    public void spectate(Player player) {
        spectating.add(player.getUniqueId());
    }

    public boolean isSpectating(Player player) {
        return spectating.contains(player.getUniqueId());
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent e){
        if(!(e.getEntity() instanceof Player)) {
            return;
        }

        if(!(e.getDamager() instanceof Arrow)) {
            return;
        }

        Arrow arrow = (Arrow) e.getDamager();
        Vector velocity = arrow.getVelocity();
        Location location = arrow.getLocation();

        Player shooter = (Player) arrow.getShooter();
        Player player = (Player) e.getEntity();

        if(!isSpectating(player)) {
            return;
        }

        e.setCancelled(true);

        player.teleport(player.getLocation().add(0, 5, 0));
        player.setFlying(true);

        arrow.remove();

        arrow = player.getWorld().spawn(location, Arrow.class);
        arrow.setVelocity(velocity);
        arrow.setShooter(shooter);
        arrow.setBounce(false);
    }

}
