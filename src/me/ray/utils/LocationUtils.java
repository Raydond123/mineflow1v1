package me.ray.utils;

import me.ray.Mineflow1v1;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class LocationUtils {

    private Mineflow1v1 plugin;

    public LocationUtils(Mineflow1v1 plugin) {
        this.plugin = plugin;
    }

    public String serializeLoc(Location location) {
        return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ();
    }

    public Location deserializeLoc(String serialized) {
        String[] data = serialized.split(", ");

        WorldCreator worldCreator = new WorldCreator(data[0]);
        plugin.getServer().createWorld(worldCreator);

        return new Location(Bukkit.getWorld(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2]), Double.valueOf(data[3]));
    }

}
