package me.ray.map;

import me.ray.Mineflow1v1;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MapManager {

    private Mineflow1v1 plugin;
    private List<Map> maps = new ArrayList<>();
    private List<Integer> mapsInUse = new ArrayList<>();

    public MapManager(Mineflow1v1 plugin) {
        this.plugin = plugin;
    }

    public void loadMaps() {
        File rawMapFile = new File("plugins/Mineflow1v1", "maps.yml");

        if(!rawMapFile.exists()) {
            try {
                if(rawMapFile.createNewFile()) {
                    System.out.println("Successfully creates maps.yml");
                } else {
                    System.out.println("The plugin was unable to create maps.yml");
                }
            } catch (IOException e) {
                System.out.println("An error occurred whilst creating maps.yml");
                e.printStackTrace();
            }
        }

        FileConfiguration mapFile = YamlConfiguration.loadConfiguration(rawMapFile);

        for(String mapName : mapFile.getConfigurationSection("maps").getKeys(false)) {
            Location loc1 = plugin.getLocationUtils().deserializeLoc(mapFile.getString("maps." + mapName + ".loc1"));
            Location loc2 = plugin.getLocationUtils().deserializeLoc(mapFile.getString("maps." + mapName + ".loc2"));

            if(!loc1.getWorld().getName().equals(loc2.getWorld().getName())) {
                System.out.println("An error occurred whilst loading the map: " + mapName);
                continue;
            }

            Map map = new Map();
            map.setLoc1(loc1);
            map.setLoc2(loc2);

            maps.add(map);
        }

        Collections.shuffle(maps);
    }

    public Map getNextMapAvailable() {
        for(int i = 0; i < maps.size(); i++) {
            if(mapsInUse.contains(i)) continue;
            return maps.get(i);
        }

        return null;
    }

    public void openMap(Map map) {
        int index = maps.indexOf(map);
        mapsInUse.remove(Integer.valueOf(index));
    }

}
