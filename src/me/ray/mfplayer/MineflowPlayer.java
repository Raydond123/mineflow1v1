package me.ray.mfplayer;

import org.bukkit.entity.Player;

public class MineflowPlayer {

    private Player player;
    private boolean inMatch = false;
    private boolean spectating = false;
    private Logout logout = null;
    private int elo = 0;

    public MineflowPlayer(Player player) {
         this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isInMatch() {
        return inMatch;
    }

    public void setInMatch(boolean inMatch) {
        this.inMatch = inMatch;
    }

    public int getElo() {
        return elo;
    }

    public void setElo(int elo) {
        this.elo = elo;
    }

    public boolean isSpectating() {
        return spectating;
    }

    public void setSpectating(boolean spectating) {
        this.spectating = spectating;
    }

    public void setLogout(Logout logout) {
        this.logout = logout;
    }

    public Logout getLogout() {
        return logout;
    }

}
