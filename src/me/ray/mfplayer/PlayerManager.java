package me.ray.mfplayer;

import me.ray.Mineflow1v1;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager implements Listener {

    private Mineflow1v1 plugin;
    private Map<UUID, MineflowPlayer> playerMap = new HashMap<>();

    public PlayerManager(Mineflow1v1 plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        if(playerMap.containsKey(player.getUniqueId())) {
            return;
        }

        MineflowPlayer mfPlayer = new MineflowPlayer(player);
        mfPlayer.setElo(plugin.getEloManager().getElo(player));

        playerMap.put(player.getUniqueId(), mfPlayer);
    }

    public MineflowPlayer getMfPlayer(Player player) {
        return playerMap.get(player.getUniqueId());
    }

    public void updateMfPlayer(MineflowPlayer mfPlayer) {
        UUID uuid = mfPlayer.getPlayer().getUniqueId();
        playerMap.remove(uuid);
        playerMap.put(uuid, mfPlayer);
    }

    public void removeMfPlayer(Player player) {
        playerMap.remove(player.getUniqueId());
    }

}
