package me.ray.language;

import me.ray.utils.Utils;

public enum Language {

    KILLED("&e%player% has died!"),
    STARTING_RANKED_1V1_MATCH("&eStarting Ranked Match:\n &e- &c%player1%&e(&c%elo1%&e)\n &e- &c%player2%&e(&c%elo2%&e)"),
    STARTING_UNRANKED_1V1_MATCH("&eStarting Unranked Match:\n &e- &c%player1%\n &e- &c%player2%"),
    STARTING_UNRANKED_TEAM_MATCH("&eStarting Unranked Team Match:\n &e- &c%team1%\n &e- &c%team2%"),
    RANKED_1V1_END("&eWinner: &c%playerwin%&e(&c%elowin%&e)\n&eLoser: &c%playerlose%&e(&c%elolose%&e)"),
    UNRANKED_1V1_END("&eWinner: &c%playerwin%\n&eLoser: &c%playerlose%"),
    UNRANKED_TEAM_END("&eWinners: &c%teamwin%\n&eLosers: &c%teamlose%"),
    NO_PERMISSION("&cYou do not have permission to run this command!"),
    CORRECT_USAGE("&cCorrect Usage: &e%usage%"),
    KIT_SET("&cSuccessfully set the kit: &e\"%kit%\"\n&cBe sure to reload the server."),
    KIT_REMOVED("&cSuccessfully remove the kit: &e\"%kit%\"\n&cBe sure to relaod the server."),
    TEAM_LEAVE("&e%player% &chas logged off and left your team!"),
    TEAM_DISBAND("&cYour team has been disbanded!"),
    CMD_FORMAT("&cFormat: &e%format%"),
    NOT_IN_TEAM("&cYou're not in a team!");

    private String message;

    Language(String message) {
        this.message = message;
    }

    public String getMessage() {
        return Utils.color(message);
    }

}
