package me.ray.match;

public enum MatchType {

    RANKED_1V1,
    UNRANKED_1v1,
    UNRANKED_TEAM;

}
