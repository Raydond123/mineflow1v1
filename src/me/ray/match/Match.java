package me.ray.match;

import me.ray.Mineflow1v1;
import me.ray.kit.Kit;
import me.ray.language.Language;
import me.ray.map.Map;
import me.ray.mfplayer.MineflowPlayer;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class Match {

    private Mineflow1v1 plugin;
    private List<MineflowPlayer> players1;
    private List<MineflowPlayer> players2;
    private List<MineflowPlayer> deadPlayers = new ArrayList<>();
    private Kit kit;
    private Map map;
    private MatchType matchType;
    private boolean ranked;

    public Match(Mineflow1v1 plugin) {
        this.plugin = plugin;
    }

    public void start() {
        //Teleport players1
        for(MineflowPlayer mfPlayer : players1) {
            Player player = mfPlayer.getPlayer();
            player.teleport(map.getLoc1());
        }

        //Teleport players2
        for(MineflowPlayer mfPlayer : players2) {
            Player player = mfPlayer.getPlayer();
            player.teleport(map.getLoc2());
        }

        //Put on kits, send messages, and play sound
        for(MineflowPlayer mfPlayer : getAllPlayers()) {
            mfPlayer.setInMatch(true);
            plugin.getPlayerManager().updateMfPlayer(mfPlayer);

            Player player = mfPlayer.getPlayer();

            player.getInventory().clear();
            player.getInventory().setArmorContents(null);

            java.util.Map<Integer, ItemStack> kitInv = kit.getPlayerInv(player);
            for(int i : kitInv.keySet()) {
                player.getInventory().setItem(i, kitInv.get(i));
            }
            player.updateInventory();

            player.playSound(player.getLocation(), Sound.NOTE_PLING, 0, 1);

            if(matchType == MatchType.RANKED_1V1) {

                player.sendMessage(Language.STARTING_RANKED_1V1_MATCH.getMessage()
                        .replace("%player1%", players1.get(0).getPlayer().getName())
                        .replace("%player2%", players2.get(0).getPlayer().getName())
                        .replace("%elo1%", players1.get(0).getElo() + "")
                        .replace("%elo2%", players2.get(0).getElo() + ""));

            } else if (matchType == MatchType.UNRANKED_1v1) {

                player.sendMessage(Language.STARTING_UNRANKED_1V1_MATCH.getMessage()
                    .replace("%player1%", players1.get(0).getPlayer().getName())
                    .replace("%player2%", players2.get(0).getPlayer().getName()));

            } else if (matchType == MatchType.UNRANKED_TEAM) {

                String team1 = "";
                String team2 = "";

                for (MineflowPlayer mfp : players1) {
                    team1 += mfp.getPlayer().getName() + ", ";
                }
                for (MineflowPlayer mfp : players2) {
                    team2 += mfp.getPlayer().getName() + ", ";
                }

                team1 = team1.substring(0, team1.length() - 2);
                team2 = team2.substring(0, team2.length() - 2);

                player.sendMessage(Language.STARTING_UNRANKED_TEAM_MATCH.getMessage()
                        .replace("%team1%", team1)
                        .replace("%team2%", team2));

            }
        }
    }

    public void cleanup() {
        List<MineflowPlayer> winner;
        List<MineflowPlayer> loser;

        if(deadPlayers.containsAll(players1)) {
            winner = players2;
            loser = players1;
        } else {
            winner = players1;
            loser = players2;
        }

        if(matchType == MatchType.RANKED_1V1) {

            int eloChange = plugin.getEloManager().calcWinLoseElo(winner.get(0).getElo(), loser.get(0).getElo());

            winner.get(0).setElo(winner.get(0).getElo() + eloChange);
            loser.get(0).setElo(loser.get(0).getElo() - eloChange);

            plugin.getPlayerManager().updateMfPlayer(winner.get(0));
            plugin.getPlayerManager().updateMfPlayer(loser.get(0));

            plugin.getEloManager().saveElo(winner.get(0).getPlayer(), winner.get(0).getElo());
            plugin.getEloManager().saveElo(loser.get(0).getPlayer(), loser.get(0).getElo());

            for(MineflowPlayer mfp : getAllPlayers()) {
                mfp.getPlayer().sendMessage(Language.RANKED_1V1_END.getMessage()
                    .replace("%playerwin%", winner.get(0).getPlayer().getName()
                    .replace("%playerlose%", loser.get(0).getPlayer().getName()
                    .replace("%elowin%", winner.get(0).getElo() + "")
                    .replace("%elolose%", loser.get(0).getElo() + ""))));
            }
            
        } else if(matchType == MatchType.UNRANKED_1v1) {
            
            for(MineflowPlayer mfp : getAllPlayers()) {
                mfp.getPlayer().sendMessage(Language.UNRANKED_1V1_END.getMessage()
                    .replace("%playerwin%", winner.get(0).getPlayer().getName()
                    .replace("%playerlose%", loser.get(0).getPlayer().getName())));
            }
            
        } else if(matchType == MatchType.UNRANKED_TEAM) {

            String teamwin = "";
            String teamlose = "";

            for (MineflowPlayer mfp : winner) {
                teamwin += mfp.getPlayer().getName() + ", ";
            }
            for (MineflowPlayer mfp : loser) {
                teamlose += mfp.getPlayer().getName() + ", ";
            }

            teamwin = teamwin.substring(0, teamwin.length() - 2);
            teamlose = teamlose.substring(0, teamlose.length() - 2);

            for(MineflowPlayer mfp : getAllPlayers()) {
                mfp.getPlayer().sendMessage(Language.UNRANKED_TEAM_END.getMessage()
                    .replace("%teamwin%", teamwin)
                    .replace("%teamlose%", teamlose));
            }
            
        }

        for(MineflowPlayer mfp : getAllPlayers()) {
            final Player player = mfp.getPlayer();

            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.updateInventory();

            player.setGameMode(GameMode.CREATIVE);

            new BukkitRunnable() {
                @Override
                public void run() {
                    player.teleport(Mineflow1v1.getPlugin(Mineflow1v1.class).getSpawn());
                }
            }.runTaskLater(Mineflow1v1.getPlugin(Mineflow1v1.class), 3 * 20);
        }
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }

    public void setRanked(boolean ranked) {
        this.ranked = ranked;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public void setPlayers1(List<MineflowPlayer> players1) {
        this.players1 = players1;
    }

    public void setPlayers2(List<MineflowPlayer> players2) {
        this.players2 = players2;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Map getMap() {
        return map;
    }

    public List<MineflowPlayer> getAllPlayers() {
        List<MineflowPlayer> list = new ArrayList<>(players1);
        list.addAll(players2);
        return list;
    }

    public void addDeath(MineflowPlayer player) {
        deadPlayers.add(player);

        for(MineflowPlayer mfPlayer : getAllPlayers()) {
            mfPlayer.getPlayer().sendMessage(Language.KILLED.getMessage().replace("%player%", player.getPlayer().getName()));
        }
    }

    public boolean shouldEnd() {
        return deadPlayers.containsAll(players1) || deadPlayers.containsAll(players2);
    }

    public boolean isRanked() {
        return ranked;
    }
}
