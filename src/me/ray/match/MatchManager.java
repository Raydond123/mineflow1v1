package me.ray.match;

import me.ray.Mineflow1v1;
import me.ray.kit.Kit;
import me.ray.language.Language;
import me.ray.map.Map;
import me.ray.mfplayer.MineflowPlayer;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class MatchManager implements Listener {

    private Mineflow1v1 plugin;
    private List<Match> matches = new ArrayList<>();

    public MatchManager(Mineflow1v1 plugin) {
        this.plugin = plugin;
    }

    public void createMatch(List<Player> players1, List<Player> players2, Kit kit, boolean ranked) {
        Map map = plugin.getMapManager().getNextMapAvailable();

        List<MineflowPlayer> mfPlayers1 = new ArrayList<>();
        List<MineflowPlayer> mfPlayers2 = new ArrayList<>();

        for(Player player : players1) {
            MineflowPlayer mfPlayer = plugin.getPlayerManager().getMfPlayer(player);
            mfPlayers1.add(mfPlayer);
        }

        for(Player player : players2) {
            MineflowPlayer mfPlayer = plugin.getPlayerManager().getMfPlayer(player);
            mfPlayers2.add(mfPlayer);
        }

        Match match = new Match(plugin);
        match.setKit(kit);
        match.setMap(map);
        match.setRanked(ranked);
        match.setPlayers1(mfPlayers1);
        match.setPlayers2(mfPlayers2);

        if(ranked) {
            match.setMatchType(MatchType.RANKED_1V1);
        } else {
            if(mfPlayers1.size() > 1 || mfPlayers2.size() > 1) {
                match.setMatchType(MatchType.UNRANKED_TEAM);
            } else {
                match.setMatchType(MatchType.UNRANKED_1v1);
            }
        }

        match.start();

        matches.add(match);
    }

    public Match getMatch(Player player) {
        MineflowPlayer mfPlayer = plugin.getPlayerManager().getMfPlayer(player);

        for(int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);

            if(match.getAllPlayers().contains(mfPlayer)) {
                return match;
            }
        }

        return null;
    }

    public void removeMatch(Match match) {
        matches.remove(match);
    }

    /**
     * On death
     */
    @EventHandler
    public void onDeath(EntityDamageByEntityEvent e) {
        if(!(e.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) e.getEntity();

        if(player.getHealth() - e.getFinalDamage() > 0) {
            return;
        }

        MineflowPlayer mfPlayer = plugin.getPlayerManager().getMfPlayer(player);

        if(mfPlayer.isInMatch()) {
            mfPlayer.setSpectating(true);
            plugin.getPlayerManager().updateMfPlayer(mfPlayer);

            e.setCancelled(true);

            Match match = getMatch(player);
            match.addDeath(mfPlayer);

            player.setFlying(true);

            player.setGameMode(GameMode.CREATIVE);

            plugin.getSpectateUtil().isSpectating(player);

            if (match.shouldEnd()) {
                match.cleanup();
                plugin.getMapManager().openMap(match.getMap());
                removeMatch(match);
            } else {
                List<MineflowPlayer> players = match.getAllPlayers();

                for(MineflowPlayer mfp : players) {
                    mfp.getPlayer().sendMessage(Language.KILLED.getMessage().replace("%player%", player.getName()));
                }

                for(ItemStack item : player.getInventory().getContents()) {
                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), item);
                }
            }

            player.teleport(player.getLocation().add(0, 5, 0));

            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.updateInventory();
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        MineflowPlayer mfPlayer = plugin.getPlayerManager().getMfPlayer(player);

        if(mfPlayer.isInMatch()) {
            if(mfPlayer.isSpectating()) {
                player.teleport(plugin.getSpawn());
                return;
            }

            Match match = getMatch(player);
            player.setHealth(0);

            match.addDeath(mfPlayer);

            if(match.shouldEnd()) {
                match.cleanup();
                plugin.getMapManager().openMap(match.getMap());
                removeMatch(match);
            } else {
                for(MineflowPlayer mfp : match.getAllPlayers()) {
                    mfp.getPlayer().sendMessage(Language.KILLED.getMessage().replace("%player%", player.getName()));
                }
            }

            return;
        }

        plugin.getPlayerManager().removeMfPlayer(player);
    }

}
