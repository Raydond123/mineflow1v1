package me.ray.team;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Team {

    private List<UUID> players = new ArrayList<>();
    private List<String> invited = new ArrayList<>();
    private Player creator;
    private boolean inMatch = false;

    public Team(Player creator) {
        this.creator = creator;
    }

    public List<UUID> getPlayers() {
        return players;
    }

    public void addPlayer(Player player) {
        players.add(player.getUniqueId());
    }

    public void removePlayer(Player player) {
        players.add(player.getUniqueId());
    }

    public boolean containsPlayer(Player player) {
        return players.contains(player.getUniqueId());
    }

    public Player getCreator() {
        return creator;
    }

    public void addInvite(Player player) {
        invited.add(player.getName());
    }

    public boolean isInvited(Player player) {
        return invited.contains(player.getName());
    }

    public void removeInvite(Player player) {
        invited.remove(player.getName());
    }

    public void setInMatch(boolean value) {
        this.inMatch = value;
    }

    public boolean isInMatch() {
        return inMatch;
    }

}
