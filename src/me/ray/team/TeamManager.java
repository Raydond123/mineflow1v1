package me.ray.team;

import me.ray.language.Language;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TeamManager implements Listener {

    private Map<UUID, Team> teams = new HashMap<>();

    public void createTeam(Player player) {
        Team team = new Team(player);
        team.addPlayer(player);

        teams.put(player.getUniqueId(), team);
    }

    public void removeTeam(Player player) {
        teams.remove(player.getUniqueId());
    }

    public boolean isInTeam(Player player) {
        for(Team team : teams.values()) {
            if(team.getPlayers().contains(player.getUniqueId())) {
                return true;
            }
        }

        return false;
    }

    public boolean isOwnerOfATeam(Player player) {
        return teams.containsKey(player.getUniqueId());
    }

    public Team getTeam(Player player) {
        for(Team team : teams.values()) {
            if(team.containsPlayer(player)) {
                return team;
            }
        }

        return null;
    }

    public void updateTeam(Team team) {
        for(UUID uuid : teams.keySet()) {
            if(teams.get(uuid).getCreator().getUniqueId() == team.getCreator().getUniqueId()) {
                teams.remove(uuid);
                teams.put(uuid, team);
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        if(!isInTeam(player)) {
            return;
        }

        Team team = getTeam(player);

        if(team.getCreator().getUniqueId().equals(player.getUniqueId())) {
            for(UUID uuid : team.getPlayers()) {
                Player p = Bukkit.getPlayer(uuid);

                if(p != null) {
                    p.sendMessage(Language.TEAM_DISBAND.getMessage());
                }
            }

            teams.remove(player.getUniqueId());
        } else {
            team.removePlayer(player);

            team.getCreator().sendMessage(Language.TEAM_LEAVE.getMessage().replace("%player%", player.getName()));

            updateTeam(team);
        }
    }

}
