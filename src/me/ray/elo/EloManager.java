package me.ray.elo;

import me.ray.utils.FileUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class EloManager {

    public void saveElo(Player player, int newElo) {
        FileConfiguration elos = FileUtils.getFile("elo.yml");
        elos.set("elo." + player.getUniqueId(), newElo);
        FileUtils.saveFile(elos, "elo.yml");
    }

    public int getElo(Player player) {
        FileConfiguration elos = FileUtils.getFile("elo.yml");
        return elos.getInt("elo." + player.getUniqueId());
    }

    public int calcWinLoseElo(int winner, int loser) {
        int k = 40;
        int difference = loser - winner;
        double percentage = 1.0 / ( 1 + Math.pow( 10, (double) difference / 400 ));
        return (int) Math.round(k * (1 - percentage));
    }

}
