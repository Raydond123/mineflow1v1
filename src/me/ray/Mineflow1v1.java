package me.ray;

import me.ray.commands.KitCMD;
import me.ray.elo.EloManager;
import me.ray.kit.KitManager;
import me.ray.map.MapManager;
import me.ray.match.MatchManager;
import me.ray.mfplayer.PlayerManager;
import me.ray.team.TeamManager;
import me.ray.utils.LocationUtils;
import me.ray.utils.SpectateUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
*COPYRIGHT 2015 : Raymond Nguyen
*ALL RIGHTS RESERVED
 */

public class Mineflow1v1 extends JavaPlugin {

    /**
     * Managers
     */
    private MapManager mapManager;
    private MatchManager matchManager;
    private PlayerManager playerManager;
    private EloManager eloManager;
    private TeamManager teamManager;

    private LocationUtils locationUtils;
    private SpectateUtil spectateUtil;

    private Location SPAWN;

    public void onEnable() {
        locationUtils = new LocationUtils(this);
        spectateUtil = new SpectateUtil();

        mapManager = new MapManager(this);
        KitManager kitManager = new KitManager();
        playerManager = new PlayerManager(this);
        matchManager = new MatchManager(this);
        eloManager = new EloManager();
        teamManager = new TeamManager();

        registerEvents();
        registerCommands();

        mapManager.loadMaps();
        kitManager.loadInventories();

        SPAWN = locationUtils.deserializeLoc(getConfig().getString("spawn"));
    }

    private void registerCommands() {
        getCommand("kit").setExecutor(new KitCMD());
    }

    private void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(playerManager, this);
        pm.registerEvents(matchManager, this);
        pm.registerEvents(spectateUtil, this);
    }

    public LocationUtils getLocationUtils() {
        return locationUtils;
    }

    public SpectateUtil getSpectateUtil() {
        return spectateUtil;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

    public MatchManager getMatchManager() {
        return matchManager;
    }

    public EloManager getEloManager() {
        return eloManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public TeamManager getTeamManager() {
        return teamManager;
    }

    public Location getSpawn() {
        return SPAWN;
    }
}
