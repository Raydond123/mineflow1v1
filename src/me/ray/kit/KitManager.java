package me.ray.kit;

import me.ray.utils.FileUtils;
import me.ray.utils.InventoryUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class KitManager {

    private List<Kit> kits = new ArrayList<>();

    public void loadInventories() {
        FileConfiguration invFile = FileUtils.getFile("kits.yml");

        for(String kitName : invFile.getConfigurationSection("kits").getKeys(false)) {
            Map<Integer, ItemStack> defaultInv = InventoryUtils.deseralizeInv(invFile.getString("kits." + kitName + ".default"));
            Map<UUID, Map<Integer, ItemStack>> inventories = new HashMap<>();

            for(String rawUUID : invFile.getConfigurationSection("kits." + kitName + ".custom").getKeys(false)) {
                UUID uuid = UUID.fromString(rawUUID);
                Map<Integer, ItemStack> inventory = InventoryUtils.deseralizeInv(invFile.getString("kits." + kitName + ".custom." + rawUUID));
                inventories.put(uuid, inventory);
            }

            Kit kit = new Kit();
            kit.setName(kitName);
            kit.setDefaultInv(defaultInv);
            kit.setInventories(inventories);

            kits.add(kit);
        }
    }

}
