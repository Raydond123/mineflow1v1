package me.ray.kit;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.UUID;

public class Kit {

    private Map<UUID, Map<Integer, ItemStack>> inventories;
    private Map<Integer, ItemStack> defaultInv;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setInventories(Map<UUID, Map<Integer, ItemStack>> inventories) {
        this.inventories = inventories;
    }

    public void setDefaultInv(Map<Integer, ItemStack> defaultInv) {
        this.defaultInv = defaultInv;
    }

    public Map<Integer, ItemStack> getPlayerInv(Player player) {
        return inventories.containsKey(player.getUniqueId()) ? inventories.get(player.getUniqueId()) : defaultInv;
    }

}
