package me.ray.commands;

import me.ray.Mineflow1v1;
import me.ray.language.Language;
import me.ray.team.Team;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamCMD implements CommandExecutor {

    private Mineflow1v1 plugin;

    public TeamCMD(Mineflow1v1 plugin) {
        this.plugin = plugin;
    }

    /**
     * Command for joining/leaving teams
     * leave
     * join [player]
     * invite [player]
     * info
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(!(sender instanceof Player)) {
            System.out.println("Only players can run this command!");
            return false;
        }

        Player player = (Player) sender;

        if(args.length == 0) {
            player.sendMessage(Language.CMD_FORMAT.getMessage().replace("%format%", "/team [leave/join/invite/info] [arg]"));
            return false;
        } else if(args.length == 1) {
            if(args[0].equalsIgnoreCase("leave")) {
                if(!plugin.getTeamManager().isInTeam(player)) {
                    player.sendMessage(Language.NOT_IN_TEAM.getMessage());
                    return false;
                } else {
                    if(plugin.getTeamManager().isOwnerOfATeam(player)) {
                        plugin.getTeamManager().removeTeam(player);
                        return false;
                    } else {
                        Team team = plugin.getTeamManager().getTeam(player);
                        team.removePlayer(player);
                        plugin.getTeamManager().updateTeam(team);
                        return false;
                    }
                }
            } else if(args[0].equalsIgnoreCase("info")) {

            }
        }

        return true;
    }

}
