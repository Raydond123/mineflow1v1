package me.ray.commands;

import me.ray.language.Language;
import me.ray.utils.FileUtils;
import me.ray.utils.InventoryUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class KitCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(!(sender instanceof Player)) {
            System.out.println("This command can only be run by a player!");
            return false;
        }

        Player player = (Player) sender;

        if(!player.hasPermission("mineflow1v1.kit")) {
            player.sendMessage(Language.NO_PERMISSION.getMessage());
            return false;
        }

        if(args.length != 2) {
            player.sendMessage(Language.CORRECT_USAGE.getMessage().replace("%usage%", "/kit [add|remove] [kit-name]"));
            return false;
        }

        if(args[0].equalsIgnoreCase("add")) {
            Map<Integer, ItemStack> inventory = new HashMap<>();

            for (int i = 0; i < 36; i++) {
                inventory.put(i, player.getInventory().getItem(i));
            }

            String serialized = InventoryUtils.serealizeInv(inventory);

            FileConfiguration kits = FileUtils.getFile("kits.yml");
            kits.set("kits." + args[1] + ".default", serialized);

            FileUtils.saveFile(kits, "kits.yml");

            player.sendMessage(Language.KIT_SET.getMessage().replace("%kit%", args[1]));
        } else if(args[0].equalsIgnoreCase("remove")) {
            FileConfiguration kits = FileUtils.getFile("kits.yml");
            kits.set("kits." + args[1], null);

            FileUtils.saveFile(kits, "kits.yml");

            player.sendMessage(Language.KIT_REMOVED.getMessage().replace("%kit%", args[1]));
        } else {
            player.sendMessage(Language.CORRECT_USAGE.getMessage().replace("%usage%", "/kit [add|remove] [kit-name]"));
            return false;
        }

        return true;
    }

}
